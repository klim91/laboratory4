try:
    file_text = ""
    with open("Text.txt", "r") as file:
        file_text = file.read()

    #Використання функції поділу рядка за кінцем рядка
    split_by_endlines = file_text.splitlines()
    #Заповнення масиву реченнями з нумерацією кожного з них на початку
    split_by_endlines = [str(split_by_endlines.index(sentence) + 1) + " " + sentence for sentence in split_by_endlines]
    
    #Виведення тексту з індексом кожного речення на початку
    print("Each sentence with id at the beginning:")
    print("".join(split_by_endlines))

    #Використання функції для заміни літери t на табуляцію
    text_with_tabs = file_text.replace("t", "\t")

    #Вивід тексту з табуляціями замість літери t
    print("\nEach e in text replaced with '\\t' :\n")
    print(text_with_tabs)

    #Використання функції для зміни кількості пробілів для табуляцій в тексті
    text_with_expanded_tabs = text_with_tabs.expandtabs(5)
    
    #Виведення тексту зі зміненим розміром табуляцій
    print("Tabs in text expanded to 5 spaces:\n")
    print(text_with_expanded_tabs)

 #Функція, в якій за допомогою методу find() знаходиться перша поява вказаного слова
    find_the_word=file_text.find("forest")
    print(find_the_word)
    #Функція, в якій за допомогою методу count() підраховується кількість вказаного слова у тексті
    count_the_words=file_text.count("leaves")
    print(count_the_words)
    #Функція, в якій за допомогою методу partition() повертається текст де його розділено на 3 частини(1-до заданого слова 2-задане слово 3-після заданого слова) 
    tuple_with_three_elements=file_text.partition("melodies")
    print(tuple_with_three_elements)

 #Функція, в якій за допомогою методу capitalize() перші символи у кожному рядку перетворюються з маліх у великі
    first_character_upper = file_text.capitalize ()
    print(first_character_upper)

    #Функція, в якій за допомогою методу center() повертає відцентрований рядок, по краях якого стоїть довільний символ
    text_in_center = file_text.center (1000)
    print("\n")
    print(text_in_center)

    #Функція, в якій за допомогою методу zfill() дозволяє додати провідні нулі до рядка
    text_with_null = file_text.zfill (900)
    print("\n")
    print(text_with_null)

    #Москаленко 

    print('\n')
    
   #Переведення всього тексту у верхній регістр
    text_lover = file_text.upper()
    print(text_lover)
    print('\n')

   #Переведення всього тексту на нижній регістр
    text_title = file_text.casefold()
    print(text_title)
    print('\n')

   #Метод за допомогою якого відбувається ділення текстау по комах
    text_index = file_text.split(",")
    print(text_index)
    print('\n')

except OSError:
    print("Something went wrong!!!")
